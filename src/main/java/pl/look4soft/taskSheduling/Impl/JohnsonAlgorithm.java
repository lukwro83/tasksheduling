package pl.look4soft.taskSheduling.Impl;

import pl.look4soft.taskSheduling.Model.TaskIn2Machine;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class JohnsonAlgorithm {


    public static List<TaskIn2Machine> sheduling(List<TaskIn2Machine> tasks) {
        TaskIn2Machine[] sheduled = new TaskIn2Machine[tasks.size()];
        int start = 0;
        int end = tasks.size() - 1;
        int tableSize = tasks.size();
        for (int i = 0; i < tableSize; i++) {
            int shortest = findShortestTasks(tasks);
            if (tasks.get(shortest).getTimeFirstMachine() <= tasks.get(shortest).getTimeSecondMachine()) {
                sheduled[start] = tasks.get(shortest);
                start++;
            } else {
                sheduled[end] = tasks.get(shortest);
                end--;
            }
            tasks.remove(shortest);
        }
        return Arrays.asList(sheduled);
    }

    private static int findShortestTasks(List<TaskIn2Machine> sheduled) {
        int shortest = calculateTime(sheduled.get(0));
        int id = 0;
        for (int i = 0; i < sheduled.size(); i++) {
            int actual = calculateTime(sheduled.get(i));
            if (actual < shortest) {
                shortest = actual;
                id = i;
            }
        }
        return id;

    }

    private static int calculateTime(TaskIn2Machine taskIn2Machine) {
        return taskIn2Machine.getTimeSecondMachine() + taskIn2Machine.getTimeFirstMachine();
    }


    public static int checkEndTime(List<TaskIn2Machine> tasks) {
        if (tasks.size() == 0) {
            return 0;
        } else if (tasks.size() == 1) {
            return tasks.get(0).getTimeFirstMachine() + tasks.get(0).getTimeSecondMachine();
        } else {
            int lineOne = 0;
            int lineTwo = 0;
            for (int i = 0; i < tasks.size(); i++) {
                if (i == 0) {
                    lineOne += tasks.get(0).getTimeFirstMachine();
                } else {
                    if (lineTwo < lineOne) {
                        lineTwo = lineOne + tasks.get(i - 1).getTimeSecondMachine();
                    } else {
                        lineTwo += tasks.get(i - 1).getTimeSecondMachine();
                    }
                    lineOne += tasks.get(i).getTimeFirstMachine();
                }
            }
            lineTwo += tasks.get(tasks.size() - 1).getTimeSecondMachine();
            return lineOne > lineTwo ? lineOne : lineTwo;
        }

    }
}
