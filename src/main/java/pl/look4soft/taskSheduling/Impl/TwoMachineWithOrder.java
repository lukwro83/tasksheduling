package pl.look4soft.taskSheduling.Impl;

import pl.look4soft.taskSheduling.Model.TaskIn2MachineWithOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lukaszwrona on 08.09.15.
 */
public class TwoMachineWithOrder {

    public static List<TaskIn2MachineWithOrder> sheduling(List<TaskIn2MachineWithOrder> tasks) {
        List<TaskIn2MachineWithOrder> sheduled = new ArrayList<>();
        if (tasks.size() == 1) {
            return tasks;
        }

        return sheduled;
    }

    public static List<TaskIn2MachineWithOrder> findForMachine(List<TaskIn2MachineWithOrder> tasks, int machine) {
        List<TaskIn2MachineWithOrder> forProperMachine = new ArrayList<>();
        if (machine == 1) {
            for (TaskIn2MachineWithOrder task : tasks) {
                if (task.getOrder()[machine - 1] == machine) {
                    forProperMachine.add(task);
                }
            }
        } else {
            for (TaskIn2MachineWithOrder task : tasks) {
                if (task.getOrder()[0] == machine || (task.getOrder()[0] == 1 && task.getOrder()[1] == 2)) {
                    forProperMachine.add(task);
                }
            }
        }
        return forProperMachine;
    }
}
