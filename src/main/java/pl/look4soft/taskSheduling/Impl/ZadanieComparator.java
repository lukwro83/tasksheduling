package pl.look4soft.taskSheduling.Impl;

import pl.look4soft.taskSheduling.Model.TaskOneMachineWithEnd;

import java.util.Comparator;

/**
 * Created by lukaszwrona on 27.08.15.
 */

public class ZadanieComparator implements Comparator<TaskOneMachineWithEnd> {
    @Override
    public int compare(TaskOneMachineWithEnd o1, TaskOneMachineWithEnd o2) {
        if (o1 == o2) {
            return 0;
        }
        if (o1.getEndTime() < o2.getEndTime()) {
            return -1;
        } else if (o1.getEndTime() == o2.getEndTime()) {
            return 0;
        }
        return 1;
    }
}
