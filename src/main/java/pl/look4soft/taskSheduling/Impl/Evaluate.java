package pl.look4soft.taskSheduling.Impl;

import org.apache.log4j.Logger;
import pl.look4soft.taskSheduling.Model.ShedulerResultsJohnsonAlgorithm;
import pl.look4soft.taskSheduling.Model.ShedulerResultsOneMachine;
import pl.look4soft.taskSheduling.Model.TaskIn2Machine;
import pl.look4soft.taskSheduling.Model.TaskOneMachineWithEnd;
import pl.look4soft.taskSheduling.Utils.TasksGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class Evaluate {
    final static Logger logger = Logger.getLogger(Evaluate.class);

    public static List<ShedulerResultsOneMachine> sheduleOneMachine(int tasks, int maxTime, int maxEnd, int times) {
        List<ShedulerResultsOneMachine> wynikiLista = new ArrayList<>();
        for (int i = 0; i < times; i++) {
            List<TaskOneMachineWithEnd> zadania = TasksGenerator.generujZadaniaJednaMaszyna(tasks, maxTime, maxEnd);
            int iloscOpoznionychPrzedSzeregowaniem = OneMachineMinDelay.iloscOpoznionych(zadania);
            List<TaskOneMachineWithEnd> poszeregowane = OneMachineMinDelay.szeregowanieJednaMaszyna(zadania);
            int liczbaOpoznionych = OneMachineMinDelay.iloscOpoznionych(poszeregowane);
            ShedulerResultsOneMachine wyniki = new ShedulerResultsOneMachine(zadania.size(), iloscOpoznionychPrzedSzeregowaniem, liczbaOpoznionych);
            wynikiLista.add(wyniki);
        }
        return wynikiLista;
    }

    public static List<ShedulerResultsJohnsonAlgorithm> sheduleTwoMachine(int tasks, int maxTimeFirst, int maxTimeSecond, int times) {
        List<ShedulerResultsJohnsonAlgorithm> resultsJohnsonsAlgorihm = new ArrayList<>();
        for (int i = 0; i < times; i++) {
            List<TaskIn2Machine> tasksToShedule = TasksGenerator.generateTaskTwoMachine(tasks, maxTimeFirst, maxTimeSecond);
            ArrayList<TaskIn2Machine> tasksToShedule2 = new ArrayList<>(tasksToShedule.size());
            tasksToShedule2.addAll(tasksToShedule);
            int results = JohnsonAlgorithm.checkEndTime(tasksToShedule);
            List<TaskIn2Machine> afterShedule = JohnsonAlgorithm.sheduling(tasksToShedule);
            int resultsAfter = JohnsonAlgorithm.checkEndTime(afterShedule);
            if (results == resultsAfter) {
                logger.info("Time Before " + results + " and time after " + resultsAfter);
                logger.info("Task to shedule " + tasksToShedule2);
                logger.info("Task afert shedule " + afterShedule);
            }
            resultsJohnsonsAlgorihm.add(new ShedulerResultsJohnsonAlgorithm(i, tasks, results, resultsAfter));
        }
        return resultsJohnsonsAlgorihm;
    }

}
