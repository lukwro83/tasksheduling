package pl.look4soft.taskSheduling.Impl;

import pl.look4soft.taskSheduling.Model.TaskOneMachineWithEnd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lukaszwrona on 27.08.15.
 */
public class OneMachineMinDelay {


    public static List<TaskOneMachineWithEnd> szeregowanieJednaMaszyna(List<TaskOneMachineWithEnd> zadania) {
        List<TaskOneMachineWithEnd> uszeregowane = new ArrayList<>();
        List<TaskOneMachineWithEnd> pozaKolejnoscia = new ArrayList<>();
        Collections.sort(zadania, new ZadanieComparator());
        for (int i = 0; i < zadania.size(); i++) {
            uszeregowane.add(zadania.get(i));
            if (sumaUszeregowanych(uszeregowane) > uszeregowane.get(uszeregowane.size() - 1).getEndTime()) {
                usunNajdluzszyCiag(uszeregowane, pozaKolejnoscia);
            }
        }
        uszeregowane.addAll(pozaKolejnoscia);
        return uszeregowane;
    }

    private static void usunNajdluzszyCiag(List<TaskOneMachineWithEnd> zadania, List<TaskOneMachineWithEnd> pozaKolejnoscia) {
        int najdluzszy = 0;
        for (int i = 0; i < zadania.size(); i++) {
            if (zadania.get(i).getTimeFirstMachine() > najdluzszy) {
                najdluzszy = i;
            }
        }
        pozaKolejnoscia.add(zadania.get(najdluzszy));
        zadania.remove(najdluzszy);
    }

    private static int sumaUszeregowanych(List<TaskOneMachineWithEnd> zadania) {
        int czasWykonania = 0;
        for (int i = 0; i < zadania.size(); i++) {
            czasWykonania += zadania.get(i).getTimeFirstMachine();
        }

        return czasWykonania;
    }

    public static int iloscOpoznionych(List<TaskOneMachineWithEnd> zadania) {
        int iloscOpoznionych = 0;
        int calkowityCzasWykonania = 0;
        for (int i = 0; i < zadania.size(); i++) {
            calkowityCzasWykonania += zadania.get(i).getTimeFirstMachine();
            if (calkowityCzasWykonania > zadania.get(i).getEndTime()) {
                iloscOpoznionych++;
            }
        }
        return iloscOpoznionych;
    }
}
