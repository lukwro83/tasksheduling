package pl.look4soft.taskSheduling.Model;

/**
 * Created by lukaszwrona on 27.08.15.
 */
public class Task {
    private int timeFirstMachine;
    //TODO Zmienić na ang i dodać dziedziczenie

    public Task(int timeFirstMachine) {
        this.timeFirstMachine = timeFirstMachine;
    }


    public int getTimeFirstMachine() {
        return timeFirstMachine;
    }

    public void setTimeFirstMachine(int timeFirstMachine) {
        this.timeFirstMachine = timeFirstMachine;
    }

    @Override
    public String toString() {
        return "TASK{" +
                "timeFirstMachine=" + timeFirstMachine +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        return timeFirstMachine == task.timeFirstMachine;

    }

    @Override
    public int hashCode() {
        return timeFirstMachine;
    }
}
