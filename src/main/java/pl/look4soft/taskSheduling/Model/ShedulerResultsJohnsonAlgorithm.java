package pl.look4soft.taskSheduling.Model;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class ShedulerResultsJohnsonAlgorithm extends ShedulerResults {
    int id;
    int timeWithoutShedule;
    int timeAfterShedule;

    public ShedulerResultsJohnsonAlgorithm(int id, int timeWithoutShedule, int timeAfterShedule) {
        this.id = id;
        this.timeWithoutShedule = timeWithoutShedule;
        this.timeAfterShedule = timeAfterShedule;
    }

    public ShedulerResultsJohnsonAlgorithm(int id, int numberOfTasks, int timeWithoutShedule, int timeAfterShedule) {
        super(numberOfTasks);
        this.id = id;
        this.timeWithoutShedule = timeWithoutShedule;
        this.timeAfterShedule = timeAfterShedule;
    }



    public ShedulerResultsJohnsonAlgorithm(int id, int timeWithoutShedule) {
        this.id = id;
        this.timeWithoutShedule = timeWithoutShedule;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTimeWithoutShedule() {
        return timeWithoutShedule;
    }

    public void setTimeWithoutShedule(int timeWithoutShedule) {
        this.timeWithoutShedule = timeWithoutShedule;
    }

    public int getTimeAfterShedule() {
        return timeAfterShedule;
    }

    public void setTimeAfterShedule(int timeAfterShedule) {
        this.timeAfterShedule = timeAfterShedule;
    }

    @Override
    public String toString() {
        return "ShedulerResultsJohnsonAlgorithm{" +
                "id=" + id +
                ", timeWithoutShedule=" + timeWithoutShedule +
                ", timeAfterShedule=" + timeAfterShedule +
                '}';
    }
}
