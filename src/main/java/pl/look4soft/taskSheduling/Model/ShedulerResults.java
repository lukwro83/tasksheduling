package pl.look4soft.taskSheduling.Model;

/**
 * Created by lukaszwrona on 08.09.15.
 */
public class ShedulerResults {

    private int numberOfTasks;

    public int getNumberOfTasks() {
        return numberOfTasks;
    }

    public ShedulerResults() {
    }

    public void setNumberOfTasks(int numberOfTasks) {
        this.numberOfTasks = numberOfTasks;
    }

    public ShedulerResults(int numberOfTasks) {
        this.numberOfTasks = numberOfTasks;
    }
}
