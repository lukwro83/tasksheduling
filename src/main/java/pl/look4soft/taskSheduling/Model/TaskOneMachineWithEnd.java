package pl.look4soft.taskSheduling.Model;

/**
 * Created by lukaszwrona on 08.09.15.
 */
public class TaskOneMachineWithEnd extends Task {
    private int endTime;

    public TaskOneMachineWithEnd(int timeFirstMachine, int endTime) {
        super(timeFirstMachine);
        this.endTime = endTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }
}
