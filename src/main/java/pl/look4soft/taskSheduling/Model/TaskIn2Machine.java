package pl.look4soft.taskSheduling.Model;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class TaskIn2Machine extends Task {

    private int id;
    private int timeSecondMachine;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TaskIn2Machine(int id, int timeFirstMachine, int timeSecondMachine) {
        super(timeFirstMachine);
        this.id = id;
        this.timeSecondMachine = timeSecondMachine;
    }



    public int getTimeSecondMachine() {
        return timeSecondMachine;
    }

    public void setTimeSecondMachine(int timeSecondMachine) {
        this.timeSecondMachine = timeSecondMachine;
    }

    @Override
    public String toString() {
        return "TaskIn2Machine{" +
                "id=" + id +
                ", timeFirstMachine=" + super.getTimeFirstMachine() +
                ", timeSecondMachine=" + timeSecondMachine +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskIn2Machine that = (TaskIn2Machine) o;

        return id == that.id && timeSecondMachine == that.timeSecondMachine;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + timeSecondMachine;
        return result;
    }
}
