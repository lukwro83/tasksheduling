package pl.look4soft.taskSheduling.Model;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class ShedulerResultsOneMachine extends ShedulerResults {

    //TODO Zmienić na ang i dodać dziedziczenie
    private int iloscOpoznionychPrzedSzeregowaniem;
    private int iloscOpoznionychPoSzeregowaniu;

    public ShedulerResultsOneMachine(int numberOfTasks, int iloscOpoznionychPrzedSzeregowaniem, int iloscOpoznionychPoSzeregowaniu) {
        super(numberOfTasks);
        this.iloscOpoznionychPrzedSzeregowaniem = iloscOpoznionychPrzedSzeregowaniem;
        this.iloscOpoznionychPoSzeregowaniu = iloscOpoznionychPoSzeregowaniu;
    }

    public int getIloscOpoznionychPrzedSzeregowaniem() {
        return iloscOpoznionychPrzedSzeregowaniem;
    }

    public void setIloscOpoznionychPrzedSzeregowaniem(int iloscOpoznionychPrzedSzeregowaniem) {
        this.iloscOpoznionychPrzedSzeregowaniem = iloscOpoznionychPrzedSzeregowaniem;
    }

    public int getIloscOpoznionychPoSzeregowaniu() {
        return iloscOpoznionychPoSzeregowaniu;
    }

    public void setIloscOpoznionychPoSzeregowaniu(int iloscOpoznionychPoSzeregowaniu) {
        this.iloscOpoznionychPoSzeregowaniu = iloscOpoznionychPoSzeregowaniu;
    }


    public String zwrocNaKonsole() {
        return "Liczba Zadan " + super.getNumberOfTasks() + " ilosc Zadan opoznionych przed szeregowaniem " + iloscOpoznionychPrzedSzeregowaniem + " ilosc zadan opoznionych po szeregowaniu " + iloscOpoznionychPoSzeregowaniu;

    }
}
