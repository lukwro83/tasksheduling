package pl.look4soft.taskSheduling.Model;

import java.util.Arrays;

/**
 * Created by lukaszwrona on 08.09.15.
 */
public class TaskIn2MachineWithOrder extends TaskIn2Machine {

    private int[] order;

    public TaskIn2MachineWithOrder(int id, int timeFirstMachine, int timeSecondMachine, int[] order) {
        super(id, timeFirstMachine, timeSecondMachine);
        this.order = order;
    }

    public int[] getOrder() {
        return order;
    }

    public void setOrder(int[] order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TaskIn2MachineWithOrder that = (TaskIn2MachineWithOrder) o;

        return Arrays.equals(order, that.order);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(order);
        return result;
    }
}
