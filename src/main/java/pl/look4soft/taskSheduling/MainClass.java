package pl.look4soft.taskSheduling;

import pl.look4soft.taskSheduling.Impl.Evaluate;
import pl.look4soft.taskSheduling.Model.ShedulerResultsJohnsonAlgorithm;
import pl.look4soft.taskSheduling.Model.ShedulerResultsOneMachine;
import pl.look4soft.taskSheduling.Utils.CSVFileWriter;

import java.util.List;

/**
 * Created by lukaszwrona on 27.08.15.
 */
public class MainClass {

    public static void main(String[] args) {
        List<ShedulerResultsOneMachine> wynikiLista = Evaluate.sheduleOneMachine(10, 4, 12, 100);
        wynikiLista.addAll(Evaluate.sheduleOneMachine(20, 4, 12, 100));
        wynikiLista.addAll(Evaluate.sheduleOneMachine(30, 4, 12, 100));
        wynikiLista.addAll(Evaluate.sheduleOneMachine(40, 4, 12, 100));
        wynikiLista.addAll(Evaluate.sheduleOneMachine(50, 4, 12, 100));
        wynikiLista.addAll(Evaluate.sheduleOneMachine(60, 4, 12, 100));
        CSVFileWriter.writeCsvFileForOneMachine("JednaMaszyna.csv", wynikiLista);

        List<ShedulerResultsJohnsonAlgorithm> twoMachineJA = Evaluate.sheduleTwoMachine(10, 5, 5, 100);
        twoMachineJA.addAll(Evaluate.sheduleTwoMachine(20, 12, 12, 100));
        twoMachineJA.addAll(Evaluate.sheduleTwoMachine(30, 12, 12, 100));
        twoMachineJA.addAll(Evaluate.sheduleTwoMachine(40, 12, 12, 100));
        twoMachineJA.addAll(Evaluate.sheduleTwoMachine(50, 12, 12, 100));
        twoMachineJA.addAll(Evaluate.sheduleTwoMachine(60, 12, 12, 100));
        CSVFileWriter.writeCsvFileForJohnsonAlgorithm("JohnsonAlgorithm.csv", twoMachineJA);

    }
}