package pl.look4soft.taskSheduling.Utils;

import pl.look4soft.taskSheduling.Model.TaskIn2Machine;
import pl.look4soft.taskSheduling.Model.TaskIn2MachineWithOrder;
import pl.look4soft.taskSheduling.Model.TaskOneMachineWithEnd;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class TasksGenerator {

    public static List<TaskOneMachineWithEnd> generujZadaniaJednaMaszyna(int liczbaZadan, int ograniczenieCzasObslugi, int ograniczenieCzasZakonczenia) {
        List<TaskOneMachineWithEnd> zadania = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < liczbaZadan; i++) {
            TaskOneMachineWithEnd nowe = getTaskOneMachineWithEnd(ograniczenieCzasObslugi, ograniczenieCzasZakonczenia, random);
            zadania.add(nowe);
        }
        return zadania;
    }

    private static TaskOneMachineWithEnd getTaskOneMachineWithEnd(int ograniczenieCzasObslugi, int ograniczenieCzasZakonczenia, Random random) {
        int czasObslugi = random.nextInt(ograniczenieCzasObslugi) + 1;
        int czasZakonczenia = random.nextInt(ograniczenieCzasZakonczenia) + 1;
        while (czasObslugi > czasZakonczenia) {
            czasZakonczenia = random.nextInt(ograniczenieCzasZakonczenia) + 1;
        }
        return new TaskOneMachineWithEnd(czasObslugi, czasZakonczenia);
    }

    public static List<TaskIn2Machine> generateTaskTwoMachine(int tasksNumber, int maxFirstMachine, int maxSecondMachine) {
        List<TaskIn2Machine> tasks = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < tasksNumber; i++) {
            TaskIn2Machine task = getTaskIn2Machine(maxFirstMachine, maxSecondMachine, random, i);
            tasks.add(task);
        }
        return tasks;
    }

    private static TaskIn2Machine getTaskIn2Machine(int maxFirstMachine, int maxSecondMachine, Random random, int i) {
        int timeFirstMachine = random.nextInt(maxFirstMachine) + 1;
        int timeSecondMachine = random.nextInt(maxSecondMachine) + 1;
        return new TaskIn2Machine(i, timeFirstMachine, timeSecondMachine);
    }

    public static List<TaskIn2MachineWithOrder> generateTasksTwoMachineWithOrder(int tasksNumber, int maxFirstMachine, int maxSecondMachine) {
        List<TaskIn2MachineWithOrder> tasks = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < tasksNumber; i++) {
            int timeFirstMachine = random.nextInt(maxFirstMachine) + 1;
            int timeSecondMachine = random.nextInt(maxSecondMachine) + 1;
            tasks.add(new TaskIn2MachineWithOrder(i, 0, 0, getRandomOrder()));
        }
        return tasks;
    }

    private static int[] getRandomOrder() {
        int[] table = new int[2];
        Random random = new Random();
        if (random.nextInt(2) + 1 == 1) {
            table[0] = random.nextInt(2) + 1;
        } else {
            if (random.nextInt(2) + 1 == 1) {
                table[0] = 1;
                table[1] = 2;
            } else {
                table[0] = 2;
                table[1] = 1;
            }
        }
        return table;
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
    }

}
