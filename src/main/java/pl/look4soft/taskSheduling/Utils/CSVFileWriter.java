package pl.look4soft.taskSheduling.Utils;

import org.apache.log4j.Logger;
import pl.look4soft.taskSheduling.Model.ShedulerResultsJohnsonAlgorithm;
import pl.look4soft.taskSheduling.Model.ShedulerResultsOneMachine;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by lukaszwrona on 03.09.15.
 */
public class CSVFileWriter {

    final static Logger logger = Logger.getLogger(CSVFileWriter.class);
    //Delimiter used in CSV file
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    //CSV file header
    private static final String FILE_HEADER = "iloscZadan, iloscZadanOpoznionychPrzed, iloscZadanOpoznionychPo";

    private static final String FILE_HEADER_JOHNSON = "Number of tasks, without sheduling, after Sheduling";


    public static void writeCsvFileForJohnsonAlgorithm(String fileName, List<ShedulerResultsJohnsonAlgorithm> wyniki) {

        FileWriter fileWriter = null;
        final long startTime = System.currentTimeMillis();
        try {
            fileWriter = new FileWriter(fileName);

            //Write the CSV file header
            fileWriter.append(FILE_HEADER_JOHNSON);

            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);

            //Write a new student object list to the CSV file
            for (ShedulerResultsJohnsonAlgorithm wynik : wyniki) {
                fileWriter.append(String.valueOf(wynik.getNumberOfTasks()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(wynik.getTimeWithoutShedule()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(wynik.getTimeAfterShedule()));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }

            final long endTime = System.currentTimeMillis();

            logger.info("CSV file" + fileName + " was created successfully !!!");
            logger.info("Total execution time: " + (endTime - startTime));

        } catch (Exception e) {
            logger.error("Error in CsvFileWriter !!!", e);
            e.printStackTrace();
        } finally {

            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                }
                fileWriter.close();
            } catch (IOException e) {
                logger.error("Error while flushing/closing fileWriter !!!", e);
                e.printStackTrace();
            }

        }
    }

    public static void writeCsvFileForOneMachine(String fileName, List<ShedulerResultsOneMachine> wyniki) {

        FileWriter fileWriter = null;
        final long startTime = System.currentTimeMillis();

        try {
            fileWriter = new FileWriter(fileName);

            //Write the CSV file header
            fileWriter.append(FILE_HEADER);

            //Add a new line separator after the header
            fileWriter.append(NEW_LINE_SEPARATOR);

            //Write a new student object list to the CSV file
            for (ShedulerResultsOneMachine wynik : wyniki) {
                fileWriter.append(String.valueOf(wynik.getNumberOfTasks()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(wynik.getIloscOpoznionychPrzedSzeregowaniem()));
                fileWriter.append(COMMA_DELIMITER);
                fileWriter.append(String.valueOf(wynik.getIloscOpoznionychPoSzeregowaniu()));
                fileWriter.append(NEW_LINE_SEPARATOR);
            }

            final long endTime = System.currentTimeMillis();

            logger.info("CSV file " + fileName + " was created successfully !!!");
            logger.info("Total execution time: " + (endTime - startTime));
        } catch (Exception e) {
            logger.error("Error in CsvFileWriter !!!", e);
            e.printStackTrace();
        } finally {

            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                }
                fileWriter.close();
            } catch (IOException e) {
                logger.error("Error while flushing/closing fileWriter !!!", e);
                e.printStackTrace();
            }

        }
    }
}
