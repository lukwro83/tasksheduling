package pl.look4soft.taskSheduling.utils;

import org.junit.Assert;
import org.junit.Test;
import pl.look4soft.taskSheduling.Model.TaskIn2MachineWithOrder;
import pl.look4soft.taskSheduling.Utils.TasksGenerator;

import java.util.List;

/**
 * Created by lukaszwrona on 08.09.15.
 */
public class TaskGeneratorTest {

    @Test
    public void testGeneratorTwoMachineWithOrder() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(0, 0, 0);
        Assert.assertNotNull(tasks);
    }

    @Test
    public void testGeneratorTMOrderListSize() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(4, 1, 1);
        Assert.assertEquals(4, tasks.size());
    }

    @Test
    public void testGeneratorTMOrderProperId() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(4, 1, 1);
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertEquals(i, tasks.get(i).getId());
        }
    }

    @Test
    public void testGeneratorTMOrderNotExceed() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(4, 4, 4);
        for (TaskIn2MachineWithOrder task : tasks) {
            Assert.assertTrue(task.getTimeFirstMachine() < 4);
            Assert.assertTrue(task.getTimeSecondMachine() < 4);
        }
    }

    @Test
    public void testGeneratorTMOrderTableSize() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(4, 4, 4);
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertEquals(2, tasks.get(i).getOrder().length);
        }
    }

    @Test
    public void testGeneratorTMOrderGen() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(4, 4, 4);
        for (TaskIn2MachineWithOrder task : tasks) {
            Assert.assertTrue(task.getOrder()[0] <= 2 && task.getOrder()[1] <= 2);
        }
    }

    @Test
    public void testGeneratorTMNotTwoZeroes() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(4, 4, 4);
        for (TaskIn2MachineWithOrder task : tasks) {
            Assert.assertFalse(task.getOrder()[0] == 0 && task.getOrder()[1] == 0);
        }
    }

    @Test
    public void testGeneratorTMNotSame() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(10, 4, 4);
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertFalse(tasks.get(i).getOrder()[0] == tasks.get(i).getOrder()[1]);
        }
    }
}
