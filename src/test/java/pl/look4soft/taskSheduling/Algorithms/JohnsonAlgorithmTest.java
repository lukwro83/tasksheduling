package pl.look4soft.taskSheduling.Algorithms;

import org.junit.Assert;
import org.junit.Test;
import pl.look4soft.taskSheduling.Impl.JohnsonAlgorithm;
import pl.look4soft.taskSheduling.Model.TaskIn2Machine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lukaszwrona on 06.09.15.
 */
public class JohnsonAlgorithmTest {

    @Test
    public void checkEndTimeForOneTask() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 2, 3);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        Assert.assertEquals(5, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void checkEndTimeForZeroTask() {
        List<TaskIn2Machine> tasks = new ArrayList<>();
        Assert.assertEquals(0, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void checkEndTimeForTwoTasks() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 2, 3);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 3, 2);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        Assert.assertEquals(7, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void checkEndTimeForThreeTasks() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 2, 3);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 5, 2);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 7, 5);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        Assert.assertEquals(14, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void checkEndTimeForFourTasks() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 1, 2);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 4, 3);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 4, 4);
        TaskIn2Machine taskIn2Machine3 = new TaskIn2Machine(3, 1, 4);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        tasks.add(taskIn2Machine3);
        Assert.assertEquals(17, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void checkEndTimeForFourTasks2() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 2, 4);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 3, 2);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 1, 3);
        TaskIn2Machine taskIn2Machine3 = new TaskIn2Machine(3, 4, 2);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        tasks.add(taskIn2Machine3);
        Assert.assertEquals(13, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void checkEndTimeForFourTasks3() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 1, 3);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 2, 4);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 4, 2);
        TaskIn2Machine taskIn2Machine3 = new TaskIn2Machine(3, 3, 2);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        tasks.add(taskIn2Machine3);
        Assert.assertEquals(12, JohnsonAlgorithm.checkEndTime(tasks));
    }

    @Test
    public void shedulingTest() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 2, 4);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 3, 2);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 1, 3);
        TaskIn2Machine taskIn2Machine3 = new TaskIn2Machine(3, 4, 2);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        tasks.add(taskIn2Machine3);
        List<TaskIn2Machine> sheduled = JohnsonAlgorithm.sheduling(tasks);
        Assert.assertEquals(2, sheduled.get(0).getId());
        Assert.assertEquals(0, sheduled.get(1).getId());
        Assert.assertEquals(3, sheduled.get(2).getId());
        Assert.assertEquals(1, sheduled.get(3).getId());
    }

    @Test
    public void shedulingTest2() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 8, 7);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 4, 1);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 9, 5);
        TaskIn2Machine taskIn2Machine3 = new TaskIn2Machine(3, 6, 10);
        TaskIn2Machine taskIn2Machine4 = new TaskIn2Machine(4, 2, 3);
        List<TaskIn2Machine> tasks = new ArrayList<TaskIn2Machine>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        tasks.add(taskIn2Machine3);
        tasks.add(taskIn2Machine4);
        List<TaskIn2Machine> sheduled = JohnsonAlgorithm.sheduling(tasks);
        Assert.assertEquals(4, sheduled.get(0).getId());
        Assert.assertEquals(3, sheduled.get(1).getId());
        Assert.assertEquals(0, sheduled.get(2).getId());
        Assert.assertEquals(2, sheduled.get(3).getId());
        Assert.assertEquals(1, sheduled.get(4).getId());
    }

    @Test
    public void shedulingTest3() {
        TaskIn2Machine taskIn2Machine = new TaskIn2Machine(0, 4, 5);
        TaskIn2Machine taskIn2Machine1 = new TaskIn2Machine(1, 4, 1);
        TaskIn2Machine taskIn2Machine2 = new TaskIn2Machine(2, 8, 4);
        TaskIn2Machine taskIn2Machine3 = new TaskIn2Machine(3, 6, 8);
        TaskIn2Machine taskIn2Machine4 = new TaskIn2Machine(4, 2, 3);
        List<TaskIn2Machine> tasks = new ArrayList<>();
        tasks.add(taskIn2Machine);
        tasks.add(taskIn2Machine1);
        tasks.add(taskIn2Machine2);
        tasks.add(taskIn2Machine3);
        tasks.add(taskIn2Machine4);
        List<TaskIn2Machine> sheduled = JohnsonAlgorithm.sheduling(tasks);
        Assert.assertEquals(4, sheduled.get(0).getId());
        Assert.assertEquals(0, sheduled.get(1).getId());
        Assert.assertEquals(3, sheduled.get(2).getId());
        Assert.assertEquals(2, sheduled.get(3).getId());
        Assert.assertEquals(1, sheduled.get(4).getId());
    }
}



