package pl.look4soft.taskSheduling.impl;


import org.junit.Assert;
import org.junit.Test;
import pl.look4soft.taskSheduling.Impl.TwoMachineWithOrder;
import pl.look4soft.taskSheduling.Model.TaskIn2MachineWithOrder;
import pl.look4soft.taskSheduling.Utils.TasksGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lukaszwrona on 08.09.15.
 */
public class TwoMachineWithOrderTest {

    @Test
    public void test2MachineWONotNull() {
        List<TaskIn2MachineWithOrder> tasks = TasksGenerator.generateTasksTwoMachineWithOrder(10, 10, 10);
        Assert.assertNotNull(tasks);
    }

    @Test
    public void test2MachineWOOneTask() {
        List<TaskIn2MachineWithOrder> tasks = new ArrayList<>();
        tasks.add(new TaskIn2MachineWithOrder(0, 2, 2, new int[]{1, 2}));
        List<TaskIn2MachineWithOrder> sheduled = TwoMachineWithOrder.sheduling(tasks);
        Assert.assertTrue(tasks.equals(sheduled));
    }

    @Test
    public void testFindForMachine() {

        List<TaskIn2MachineWithOrder> tasks = new ArrayList<>();
        tasks.add(new TaskIn2MachineWithOrder(0, 2, 2, new int[]{1, 2}));
        Assert.assertEquals(1, TwoMachineWithOrder.findForMachine(tasks, 1).size());

    }

    @Test
    public void testFindForMachine2() {

        List<TaskIn2MachineWithOrder> tasks = new ArrayList<>();
        tasks.add(new TaskIn2MachineWithOrder(0, 2, 2, new int[]{1, 0}));
        Assert.assertEquals(1, TwoMachineWithOrder.findForMachine(tasks, 1).size());

    }

    @Test
    public void testFindForMachine3() {

        List<TaskIn2MachineWithOrder> tasks = new ArrayList<>();
        tasks.add(new TaskIn2MachineWithOrder(0, 2, 2, new int[]{2, 0}));
        Assert.assertEquals(1, TwoMachineWithOrder.findForMachine(tasks, 2).size());

    }

    @Test
    public void testFindForMachine4() {

        List<TaskIn2MachineWithOrder> tasks = new ArrayList<>();
        tasks.add(new TaskIn2MachineWithOrder(0, 2, 2, new int[]{2, 1}));
        Assert.assertEquals(1, TwoMachineWithOrder.findForMachine(tasks, 2).size());

    }
}
